<?php

namespace Omnibuy\Seb;

use Payum\Core\GatewayFactoryInterface;

class SebGatewayFactoryFactory
{
    public function getCallable()
    {
        return [$this, 'getGatewayFactory'];
    }

    public function getGatewayFactory(array $config, GatewayFactoryInterface $coreGatewayFactory)
    {
        return new SebGatewayFactory($config, $coreGatewayFactory);
    }
}
