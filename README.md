## Usage with Symfony

Please see the [example project](https://bitbucket.org/omnibuy/payum-seb-example) for usage with Symfony

## Running tests

There are currently no tests.

## License

Skeleton is released under the [MIT License](LICENSE).
