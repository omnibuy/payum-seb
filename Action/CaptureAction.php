<?php

namespace Omnibuy\Seb\Action;

use Payum\Core\Action\ActionInterface;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Reply\HttpResponse;
use Payum\Core\Request\Capture;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Request\GetHttpRequest;
use Payum\Core\Request\RenderTemplate;
use Payum\Core\Security\TokenInterface;
use Payum\Core\GatewayAwareInterface;

class CaptureAction implements ActionInterface, GatewayAwareInterface
{
    use GatewayAwareTrait;

    private $gatewayUrl;
    private $templateName;
    private $sndId;
    private $account;
    private $merchantName;
    private $userKey;
    private $bankKey;

    private $requestTypes = [
        1001 => ['VK_SERVICE', 'VK_VERSION', 'VK_SND_ID', 'VK_STAMP', 'VK_AMOUNT', 'VK_CURR', 'VK_ACC', 'VK_NAME', 'VK_REF', 'VK_MSG'],
        1011 => ['VK_SERVICE', 'VK_VERSION', 'VK_SND_ID', 'VK_STAMP', 'VK_AMOUNT', 'VK_CURR', 'VK_ACC', 'VK_NAME', 'VK_REF', 'VK_MSG', 'VK_RETURN', 'VK_CANCEL', 'VK_DATETIME'],
        1012 => ['VK_SERVICE', 'VK_VERSION', 'VK_SND_ID', 'VK_STAMP', 'VK_AMOUNT', 'VK_CURR', 'VK_REF', 'VK_MSG', 'VK_RETURN', 'VK_CANCEL', 'VK_DATETIME'],
        1002 => ['VK_SERVICE', 'VK_VERSION', 'VK_SND_ID', 'VK_STAMP', 'VK_AMOUNT', 'VK_CURR', 'VK_REF', 'VK_MSG'],
        1101 => ['VK_SERVICE', 'VK_VERSION', 'VK_SND_ID', 'VK_REC_ID', 'VK_STAMP', 'VK_T_NO', 'VK_AMOUNT', 'VK_CURR', 'VK_REC_ACC', 'VK_REC_NAME', 'VK_SND_ACC', 'VK_SND_NAME', 'VK_REF', 'VK_MSG', 'VK_T_DATE'],
        1111 => ['VK_SERVICE', 'VK_VERSION', 'VK_SND_ID', 'VK_REC_ID', 'VK_STAMP', 'VK_T_NO', 'VK_AMOUNT', 'VK_CURR', 'VK_REC_ACC', 'VK_REC_NAME', 'VK_SND_ACC', 'VK_SND_NAME', 'VK_REF', 'VK_MSG', 'VK_T_DATETIME'],
        1901 => ['VK_SERVICE', 'VK_VERSION', 'VK_SND_ID', 'VK_REC_ID', 'VK_STAMP', 'VK_REF', 'VK_MSG'],
        1911 => ['VK_SERVICE', 'VK_VERSION', 'VK_SND_ID', 'VK_REC_ID', 'VK_STAMP', 'VK_REF', 'VK_MSG']
    ];


    public function __construct(
        $gatewayUrl,
        $templateName,
        $sndId,
        $account,
        $merchantName,
        $userKey,
        $bankKey
    ) {
        $this->gatewayUrl = $gatewayUrl;
        $this->templateName = $templateName;
        $this->sndId = $sndId;
        $this->account = $account;
        $this->merchantName = $merchantName;
        $this->userKey = $userKey;
        $this->bankKey = $bankKey;
    }

    /**
     * {@inheritDoc}
     *
     * @param Capture $request
     */
    public function execute($request)
    {
        RequestNotSupportedException::assertSupports($this, $request);

        $model = ArrayObject::ensureArrayObject($request->getModel());

        $getHttpRequest = new GetHttpRequest();
        $this->gateway->execute($getHttpRequest);

        if (isset($getHttpRequest->query['done']) && $getHttpRequest->query['done']) {
            if (!$this->verifyMac($getHttpRequest->request)) {
                return;
            }

            $model['status'] = 'done';

            if (isset($getHttpRequest->request['VK_AUTO']) && $getHttpRequest->request['VK_AUTO'] === 'Y') {
                throw new HttpResponse('OK');
            } else {
                return;
            }
        }

        if (isset($getHttpRequest->query['cancel']) && $getHttpRequest->query['cancel']) {
            if (!$this->verifyMac($getHttpRequest->request)) {
                return;
            }

            $model['status'] = 'cancel';

            if (isset($getHttpRequest->request['VK_AUTO']) && $getHttpRequest->request['VK_AUTO'] === 'Y') {
                throw new HttpResponse('OK');
            } else {
                return;
            }
        }

        $fields = $this->getFields($model, $request->getToken());
        $this->gateway->execute($renderTemplate = new RenderTemplate($this->templateName, [
            'fields' => $fields,
            'url' => $this->gatewayUrl,
        ]));

        throw new HttpResponse($renderTemplate->getResult());
    }


    private function getFields(ArrayObject $model, TokenInterface $token)
    {
        $paymentTimestamp = gmdate('Y-m-d\TH:i:sO');

        $fields = [
            "VK_SERVICE" => "1011",
            "VK_VERSION" => "008",
            "VK_SND_ID" => $this->sndId,
            "VK_STAMP" => $token->getDetails()->getId(),
            "VK_AMOUNT" => $model['amount'],
            "VK_CURR" => $model['currency_code'],
            "VK_ACC" => $this->account,
            "VK_NAME" => $this->merchantName,
            "VK_REF" => "",
            "VK_MSG" => $model['description'],
            "VK_RETURN" => $token->getTargetUrl() . '?done=1',
            "VK_CANCEL" => $token->getTargetUrl() . '?cancel=1',
            "VK_DATETIME" => $paymentTimestamp,
            "VK_ENCODING" => "utf-8",
            "VK_LANG" => "EST"
        ];

        $fields["VK_MAC"] = $this->getFieldMac($fields, $this->userKey);
        return $fields;
    }

    private function getFieldMac($fields, $userKey)
    {
        $data = str_pad(mb_strlen($fields["VK_SERVICE"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_SERVICE"] .
            str_pad(mb_strlen($fields["VK_VERSION"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_VERSION"] .
            str_pad(mb_strlen($fields["VK_SND_ID"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_SND_ID"] .
            str_pad(mb_strlen($fields["VK_STAMP"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_STAMP"] .
            str_pad(mb_strlen($fields["VK_AMOUNT"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_AMOUNT"] .
            str_pad(mb_strlen($fields["VK_CURR"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_CURR"] .
            str_pad(mb_strlen($fields["VK_ACC"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_ACC"] .
            str_pad(mb_strlen($fields["VK_NAME"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_NAME"] .
            str_pad(mb_strlen($fields["VK_REF"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_REF"] .
            str_pad(mb_strlen($fields["VK_MSG"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_MSG"] .
            str_pad(mb_strlen($fields["VK_RETURN"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_RETURN"] .
            str_pad(mb_strlen($fields["VK_CANCEL"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_CANCEL"] .
            str_pad(mb_strlen($fields["VK_DATETIME"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_DATETIME"];

        $signature = 'N/A';
        openssl_sign($data, $signature, $userKey, OPENSSL_ALGO_SHA1);
        return base64_encode($signature);
    }

    public function verifyMac($request)
    {
        if (!$this->requestTypes[$request['VK_SERVICE']]) {
            return false;
        }

        $mac = '';
        foreach ($this->requestTypes[$request['VK_SERVICE']] as $tmp) {
            if (!isset($request['VK_ENCODING']) || $request['VK_ENCODING'] == 'UTF-8' || $request['VK_ENCODING'] == 'utf-8') {
                $mac .= str_pad(mb_strlen($request[$tmp], 'utf8'), 3, '0', STR_PAD_LEFT) . $request[$tmp];
            } else {
                $mac .= str_pad(strlen($request[$tmp]), 3, '0', STR_PAD_LEFT) . $request[$tmp];
            }
        }
        return openssl_verify($mac, base64_decode($request['VK_MAC']), openssl_pkey_get_public($this->bankKey));
    }

    /**
     * {@inheritDoc}
     */
    public function supports($request)
    {
        return
            $request instanceof Capture &&
            $request->getModel() instanceof \ArrayAccess;
    }
}
