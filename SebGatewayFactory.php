<?php

namespace Omnibuy\Seb;

use Omnibuy\Seb\Action\ConvertPaymentAction;
use Omnibuy\Seb\Action\CaptureAction;
use Omnibuy\Seb\Action\StatusAction;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayFactory;

class SebGatewayFactory extends GatewayFactory
{
    /**
     * {@inheritDoc}
     */
    protected function populateConfig(ArrayObject $config)
    {
        $config['payum.default_options'] = [
            'sandbox' => true,
            'live_url' => 'https://www.seb.ee/cgi-bin/ipank/ipank.p',
            'sandbox_url' => 'http://pangalinker.herokuapp.com/banklink/seb',

            'payum.template.client_side_redirect' => '@PayumSeb/Action/client_side_redirect.html.twig'
        ];
        $config->defaults($config['payum.default_options']);

        $config->defaults([
            'payum.factory_name' => 'seb',
            'payum.factory_title' => 'SEB',
            'payum.action.capture' => new CaptureAction(
                $config['sandbox'] ? $config['sandbox_url'] : $config['live_url'],
                $config['payum.template.client_side_redirect'],
                $config['snd_id'],
                $config['account'],
                $config['merchant_name'],
                $config['user_key'],
                $config['bank_key']
            ),
            'payum.action.status' => new StatusAction(),
            'payum.action.convert_payment' => new ConvertPaymentAction()
        ]);

        $config['payum.required_options'] = [
            'snd_id',
            'account',
            'merchant_name',
            'user_key',
            'bank_key'
        ];

        $config->validateNotEmpty($config['payum.required_options']);

        $config['payum.paths'] = array_replace([
            'PayumSeb' => __DIR__.'/Resources/views',
        ], $config['payum.paths'] ?: []);
    }
}
